package com.miiinus.munus.view;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.miiinus.munus.R;

public class RecordServiceFragment extends DialogFragment {

    private OnFragmentInteractionListener mListener;

    private DatePickerDialog.OnDateSetListener mDateStartListener, mDateEndListener;
    private TimePickerDialog.OnTimeSetListener mTimeStartListener, mTimeEndListener;

    // start date-time of service record
    private TextView mDateStartDisplay, mTimeStartDisplay;
    private Button mDateStartButton, mTimeStartButton;
    private int mYearStart, mMonthStart, mDayStart, mHourStart, mMinuteStart;

    // end date-time of service record
    private TextView mDateEndDisplay, mTimeEndDisplay;
    private Button mDateEndButton, mTimeEndButton;
    private int mYearEnd, mMonthEnd, mDayEnd, mHourEnd, mMinuteEnd;


    static final int TIME_START_DIALOG_ID = 1;
    static final int DATE_START_DIALOG_ID = 2;
    static final int TIME_END_DIALOG_ID = 3;
    static final int DATE_END_DIALOG_ID = 4;

    /**
     * Contructor
     */
    public static RecordServiceFragment newInstance() {
        RecordServiceFragment fragment = new RecordServiceFragment();
        return fragment;
    }

    public RecordServiceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_recordservice, container, false);

        setUpListeners();
        setUpButtons(v);


        mYearStart = 2015;
        mMonthStart = 5;
        mDayStart = 11;
        mHourStart = 12;
        mMinuteStart = 0;

        return v;
    }

    /**
     * private helper method to set up the listeners for the time and date picker dialogs
     */
    private void setUpListeners(){
        mTimeStartListener = new TimePickerDialog.OnTimeSetListener(){
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                mMinuteStart = minute;
                mHourStart = hourOfDay;

                mTimeStartDisplay.setText(prettyPrintTime(mHourStart, mMinuteStart));
            }
        };
        mTimeEndListener = new TimePickerDialog.OnTimeSetListener(){
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                mMinuteEnd = minute;
                mHourEnd = hourOfDay;

                mTimeEndDisplay.setText(prettyPrintTime(mHourEnd, mMinuteEnd));
            }
        };
        mDateStartListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                mYearStart = year;
                mMonthStart = monthOfYear + 1;
                mDayStart = dayOfMonth;

                mDateStartDisplay.setText(prettyPrintDate(mDayStart, mMonthStart, mYearStart));

            }
        };
        mDateEndListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                mYearEnd = year;
                mMonthEnd = monthOfYear + 1;
                mDayEnd = dayOfMonth;

                mDateEndDisplay.setText(prettyPrintDate(mDayEnd, mMonthEnd, mYearEnd));

            }
        };
    }

    /**
     * private helper method to set up the buttons of the view
     * @param v the view
     */
    private void setUpButtons(View v) {
        // start buttons and text views
        mTimeStartDisplay = (TextView) v.findViewById(R.id.timeStartDisplay);
        mTimeStartButton = (Button) v.findViewById(R.id.timeStartButton);

        mTimeStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog(TIME_START_DIALOG_ID).show();
            }
        });

        mDateStartDisplay = (TextView) v.findViewById(R.id.dateStartDisplay);
        mDateStartButton = (Button) v.findViewById(R.id.dateStartButton);

        mDateStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog(DATE_START_DIALOG_ID).show();
            }
        });

        // end buttons and text views
        mTimeEndDisplay = (TextView) v.findViewById(R.id.timeEndDisplay);
        mTimeEndButton = (Button) v.findViewById(R.id.timeEndButton);

        mTimeEndButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog(TIME_END_DIALOG_ID).show();
            }
        });

        mDateEndDisplay = (TextView) v.findViewById(R.id.dateEndDisplay);
        mDateEndButton = (Button) v.findViewById(R.id.dateEndButton);

        mDateEndButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog(DATE_END_DIALOG_ID).show();
            }
        });
    }

    /**
     * private helper method to create dialogs depending on which button was pressed
     * @param id the kind of dialog to be created
     * @return the dialog
     */
    private Dialog createDialog(int id) {
        switch (id) {
            case TIME_START_DIALOG_ID:
                return new TimePickerDialog(getActivity(), mTimeStartListener, mHourStart, mMinuteStart, true);
            case DATE_START_DIALOG_ID:
                return new DatePickerDialog(getActivity(), mDateStartListener, mYearStart, mMonthStart, mDayStart);
            case TIME_END_DIALOG_ID:
                return new TimePickerDialog(getActivity(), mTimeEndListener, mHourEnd, mMinuteEnd, true);
            case DATE_END_DIALOG_ID:
                return new DatePickerDialog(getActivity(), mDateEndListener, mYearEnd, mMonthEnd, mDayEnd);
        }
        return null;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private String prettyPrintDate(int d, int m, int y) {
        String s = "";
        s += (d < 10) ? "0"+d+"." : d+".";
        s += (m < 10) ? "0"+m+"." : m+".";
        s += y;
        return s;
    }

    private String prettyPrintTime(int h, int m) {
        String s = "";
        s += (h < 10) ? "0"+h+":" : h+":";
        s += (m < 10) ? "0"+m : m;
        return s;
    }
}
