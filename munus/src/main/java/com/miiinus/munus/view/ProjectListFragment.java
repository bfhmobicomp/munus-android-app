package com.miiinus.munus.view;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.miiinus.munus.R;
import com.miiinus.munus.Session;
import com.miiinus.munus.adapter.ProjectListAdapter;
import com.miiinus.munus.controller.ApiCallResultReceiver;
import com.miiinus.munus.controller.ApiCallService;
import com.miiinus.munus.controller.KeyValuePair;
import com.miiinus.munus.dao.ProjectDao;
import com.miiinus.munus.model.Project;

import java.util.ArrayList;

import static com.miiinus.munus.controller.ApiCallService.REQUEST_HEADER;
import static com.miiinus.munus.controller.ApiCallService.REQUEST_TYPE;
import static com.miiinus.munus.controller.ApiCallService.REQUEST_URL;
import static com.miiinus.munus.controller.ApiCallService.RESULT_STRING;
import static com.miiinus.munus.controller.ApiCallService.STATUS_ERROR;
import static com.miiinus.munus.controller.ApiCallService.STATUS_FINISHED;
import static com.miiinus.munus.controller.ApiCallService.STATUS_RUNNING;

public class ProjectListFragment extends ListFragment implements ApiCallResultReceiver.Receiver {


    private ApiCallResultReceiver mReceiver;
    private ProjectListAdapter mListAdapter;

    private static final String TAG = "ProjectListFragment";

    public static ProjectListFragment newInstance() {
        ProjectListFragment fragment = new ProjectListFragment();
        return fragment;
    }

    public ProjectListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // register a receiver for api calls
        mReceiver = new ApiCallResultReceiver(new Handler());
        mReceiver.setReceiver(this);

        mListAdapter = new ProjectListAdapter(getActivity(), R.layout.list_item_project, new ArrayList<Project>());
        this.setListAdapter(mListAdapter);

        // start ApiCallService
        final Intent intent = new Intent(Intent.ACTION_SYNC, null, getActivity(), ApiCallService.class);

        intent.putExtra(REQUEST_URL, ProjectDao.URL_PROJECTS_WORKINGFOR);
        intent.putExtra(REQUEST_TYPE, "get");

        // set the result receiver
        intent.putExtra("receiver", mReceiver);

        // all the information for the http-header
        ArrayList<KeyValuePair> headerValues = new ArrayList<KeyValuePair>();
        headerValues.add(new KeyValuePair("Accept", "application/json"));
        headerValues.add(new KeyValuePair("Content-Type", "application/json"));
        headerValues.add(new KeyValuePair("Authorization", Session.getInstance().token));

        intent.putExtra(REQUEST_HEADER, headerValues);

        Log.d(TAG, Session.getInstance().token);
        getActivity().startService(intent);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case STATUS_RUNNING:
                Toast.makeText(getActivity(), R.string.toast_db_start_con, Toast.LENGTH_LONG).show();
                break;

            case STATUS_FINISHED:
                Toast.makeText(getActivity(), R.string.toast_db_finsihed, Toast.LENGTH_LONG).show();
                String result = resultData.getString(RESULT_STRING);

                Log.d("Project List JSON", result); // TODO remove log
                mListAdapter.updateProjects(ProjectDao.getAllProjectsFromJson(result));

                break;
            case STATUS_ERROR:
                Toast.makeText(getActivity(), R.string.toast_db_error, Toast.LENGTH_LONG).show();
                //TODO
                break;
        }
    }



}