package com.miiinus.munus.controller;

/**
 * Created by Robin on 25.05.15
 */

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class ApiCallResultReceiver extends ResultReceiver {
    private Receiver mReceiver;

    public ApiCallResultReceiver(Handler handler){
            super(handler);
            }

    public void setReceiver(Receiver receiver){
            mReceiver=receiver;
            }

    public interface Receiver {
        public void onReceiveResult(int resultCode, Bundle resultData);

    }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (mReceiver != null) {
                mReceiver.onReceiveResult(resultCode, resultData);
            }
    }
}