package com.miiinus.munus.controller;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Robin on 25.05.15
 */
public class KeyValuePair implements Parcelable {
    private String key;
    private String value;

    public KeyValuePair(String k, String v) {
        this.key = k;
        this.value = v;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public KeyValuePair(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(value);
    }

    private void readFromParcel(Parcel in) {
        key = in.readString();
        value = in.readString();
    }

    /**
     * Needed for android to be able to create new objects, individually or as arrays.
     */
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public KeyValuePair createFromParcel(Parcel in) {
            return new KeyValuePair(in);
        }

        public KeyValuePair[] newArray(int size) {
            return new KeyValuePair[size];
        }
    };
}