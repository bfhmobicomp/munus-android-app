package com.miiinus.munus.controller;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.List;


/**
 * Created by Robin on 17.05.15
 */
public class ApiCallService extends IntentService {

    private static final String TAG = "APICallService";

    public static final String REQUEST_URL = "url";
    public static final String REQUEST_TYPE = "type";
    public static final String REQUEST_HEADER = "header";
    public static final String REQUEST_PARAMS = "params";
    public static final String RESULT_STRING = "result";

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    public ApiCallService() {
        super("ApiCallService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final ResultReceiver receiver = intent.getParcelableExtra("receiver");

        String type = intent.getStringExtra(REQUEST_TYPE);
        String url = intent.getStringExtra(REQUEST_URL);
        List<KeyValuePair> headerValues = intent.getParcelableArrayListExtra(REQUEST_HEADER);
        List<KeyValuePair> params = intent.getParcelableArrayListExtra(REQUEST_PARAMS);

        Log.d(TAG, "Jaja, intent ist angekommen.");

        OkHttpClient client = new OkHttpClient();
        Bundle b = new Bundle();

        if (type.equals("post")) {

            // add the params to the body
            FormEncodingBuilder bodyBuilder = new FormEncodingBuilder();
            if (!params.isEmpty()) {
                for (KeyValuePair kvp : params) {
                    bodyBuilder.add(kvp.getKey(), kvp.getValue());
                }
            }
            // build the body
            RequestBody requestBody = bodyBuilder.build();

            // add the header params to the builder
            Request.Builder builder = new Request.Builder();
            if (!headerValues.isEmpty()) {
                for (KeyValuePair kvp : headerValues) {
                    builder.addHeader(kvp.getKey(), kvp.getValue());
                }
            }
            // set the url & the body
            builder.url(url).post(requestBody);

            // build the request
            Request request = builder.build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                returnException(b, e, receiver);
            }
            if (!response.isSuccessful()) try {
                throw new IOException("Unexpected code " + response);
            } catch (IOException e) {
                returnException(b, e, receiver);
            }

            try {
                b.putString(RESULT_STRING, response.body().string());
                receiver.send(STATUS_FINISHED, b);

            } catch (IOException e) {
                returnException(b, e, receiver);
            }


        } else if (type.equals("get")) {
            Log.d(TAG, "GET-Request is executed"); // Log Chat

            receiver.send(STATUS_RUNNING, Bundle.EMPTY);
            /* GET REQUEST */

            Request.Builder builder = new Request.Builder();

            if (!headerValues.isEmpty()) {
                for (KeyValuePair kvp : headerValues) {
                    builder.addHeader(kvp.getKey(), kvp.getValue());
                }
            }

            /*
            if(!params.isEmpty()) {
                //TODO attach url params
            }
            */

            builder.url(url);

            Request request = builder.build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                returnException(b, e, receiver);
            }
            try {
                b.putString(RESULT_STRING, response.body().string());
                receiver.send(STATUS_FINISHED, b);
            } catch (IOException e) {
                returnException(b, e, receiver);
            }

        } else if (type.equals("put")) {
            //TODO

        } else if (type.equals("delete")) {
            //TODO
        } else {
            b.putString(Intent.EXTRA_TEXT, "Failure - No valid http request type");
            receiver.send(STATUS_ERROR, b);
        }
    }

    /**
     * If an exception is thrown, this returns the message to the receiver
     */
    private void returnException(Bundle b, Exception e, ResultReceiver receiver) {
        b.putString(Intent.EXTRA_TEXT, e.toString());
        receiver.send(STATUS_ERROR, b);
    }
}
