package com.miiinus.munus;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.miiinus.munus.view.ProjectListFragment;


public class ProjectListActivity extends ActionBarActivity /* implements DashboardFragment.OnItemSelectedListener*/ {

    private ProjectListFragment projectListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_list);

        if(findViewById(R.id.fragment_container2) != null) {
            if(savedInstanceState != null) {
                return;
            }
            ProjectListFragment projectListFragment = ProjectListFragment.newInstance();
            getFragmentManager().beginTransaction().add(R.id.fragment_container2, projectListFragment).commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /*@Override
    public void onItemSelected(long id) {
        switch ((int) id){

            case 0:
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.projectlist, projectListFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                ft.commit();
                break;
            case 1: //TODO implement menu logic
                break;
        }
    }*/
}
