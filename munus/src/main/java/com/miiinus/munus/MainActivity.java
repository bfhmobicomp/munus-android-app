package com.miiinus.munus;

import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.miiinus.munus.view.CustomerListFragment;
import com.miiinus.munus.view.DashboardFragment;
import com.miiinus.munus.view.ProjectListFragment;
import com.miiinus.munus.view.RecordListFragment;
import com.miiinus.munus.view.RecordServiceFragment;


public class MainActivity extends ActionBarActivity
        implements DashboardFragment.OnItemSelectedListener, RecordServiceFragment.OnFragmentInteractionListener
{

    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;

    private DashboardFragment dashboardFragment;
    private ProjectListFragment projectListFragment;
    private CustomerListFragment customerListFragment;
    private RecordListFragment recordListFragment;
    private RecordServiceFragment recordServiceFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        addDrawerItems();
        setupDrawer();

        // Set the fragment
        if(findViewById(R.id.fragment_container) != null) {
            if(savedInstanceState != null) {
                return;
            }
            dashboardFragment = DashboardFragment.newInstance();
            getSupportActionBar().setTitle(R.string.menu_dashboard);
            getFragmentManager().beginTransaction().add(R.id.fragment_container, dashboardFragment).commit();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void addDrawerItems() {
        String[] menuValues = {
                this.getString(R.string.menu_dashboard),
                this.getString(R.string.menu_record),
                this.getString(R.string.menu_record_list),
                this.getString(R.string.menu_project_list),
                this.getString(R.string.menu_customer_list)};
        mAdapter = new ArrayAdapter<String>(this, R.layout.drawer_list_item, menuValues);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switchFragment(id);
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * DASHBOARD FRAGMENT
     * Called when a Dashboard Menu Item is clicked
     * @param id the id of the menu item
     */
    @Override
         public void onItemSelected(long id) {
        //Added +1 as "Dashboard" is no Menu Item
        switchFragment(id+1);
    }

    /**
     * Called when either Dashboard-Menu or Tab Menu is used
     * @param id the id of the menu item
     */
    private void switchFragment(long id) {
        //TODO / FIND OUT: how to use parameters instead of magic numbers
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        switch ((int) id){
            case 0:
                ft.replace(R.id.fragment_container, dashboardFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                getSupportActionBar().setTitle(R.string.menu_dashboard);
                ft.commit();
                break;
            case 1:
                recordServiceFragment = RecordServiceFragment.newInstance();
                ft.replace(R.id.fragment_container, recordServiceFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                getSupportActionBar().setTitle(R.string.menu_record);
                ft.commit();
                break;
            case 2:
                recordListFragment = RecordListFragment.newInstance();
                ft.replace(R.id.fragment_container, recordListFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                getSupportActionBar().setTitle(R.string.menu_record_list);
                ft.commit();
                break;
            case 3:
                projectListFragment = ProjectListFragment.newInstance();
                ft.replace(R.id.fragment_container, projectListFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                getSupportActionBar().setTitle(R.string.menu_project_list);
                ft.commit();
                break;
            case 4:
                customerListFragment = CustomerListFragment.newInstance();
                ft.replace(R.id.fragment_container, customerListFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                getSupportActionBar().setTitle(R.string.menu_customer_list);
                ft.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
