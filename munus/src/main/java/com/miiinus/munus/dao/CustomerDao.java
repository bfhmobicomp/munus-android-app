package com.miiinus.munus.dao;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.miiinus.munus.model.Customer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 23.04.15
 */
public class CustomerDao extends BaseDao {
    public static final String URL_CUSTOMER_WORKINGFOR = URL_PREFIX + "/customers/all";

    public static List<Customer> getAllCustomersFromJson(String json) {
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(json);
        List<Customer> customerList = new ArrayList<>();
        if (element.isJsonArray()) {
            JsonArray customers = element.getAsJsonArray();
            for (int i = 0; i < customers.size(); i++) {
                JsonObject p = customers.get(i).getAsJsonObject();
                int id = p.get("id").getAsInt();
                String name = p.get("name").getAsString();
                String address;
                String phoneNumber;
                String email;

                try {
                    phoneNumber = p.get("phoneNumber").getAsString();
                } catch (UnsupportedOperationException e) {
                    phoneNumber = "";
                }

                try {
                    email = p.get("email").getAsString();
                } catch (UnsupportedOperationException e) {
                    email = "";
                }

                JsonObject addrObj = p.get("address").getAsJsonObject();

                String street;
                String zipCode;
                String city;

                try {
                    street =                 addrObj.get("street").getAsString();
                } catch (UnsupportedOperationException e) {
                    street = "";
                }

                try {
                    zipCode = addrObj.get("zipCode").getAsString();
                } catch (UnsupportedOperationException e) {
                    zipCode = "";
                }

                try {
                    city = addrObj.get("city").getAsString();
                } catch (UnsupportedOperationException e) {
                    city = "";
                }

                if (street.isEmpty() || zipCode.isEmpty() ||  city.isEmpty()) {
                    address = "";
                } else {
                    address = street + ", " + zipCode + " " + city;
                }
                customerList.add(new Customer(id, name, address, phoneNumber, email));
            }
        }
        return customerList;

    }


}
