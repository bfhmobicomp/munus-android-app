package com.miiinus.munus.dao;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.miiinus.munus.Session;
import com.miiinus.munus.model.Employee;

/**
 * Created by Robin on 23.04.15
 */
public class EmployeeDao {
    public Employee getEmployeeById(int id) {
        return null;
    }

    public static void setSession(String json) {
        JsonParser parser = new JsonParser();
        JsonElement e = parser.parse(json);
        Session s = Session.getInstance();
        if (e.isJsonObject()) {
            JsonObject o = e.getAsJsonObject();
            s.setToken(o.get("access_token").getAsString());
        }
    }
}
