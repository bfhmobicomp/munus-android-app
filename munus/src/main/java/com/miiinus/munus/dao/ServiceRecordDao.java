package com.miiinus.munus.dao;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.miiinus.munus.model.ServiceRecord;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Robin on 23.04.15
 */
public class ServiceRecordDao extends BaseDao {
    public static final String URL_ALL_SERVICERECORDS = URL_PREFIX + "/servicerecords/all";
    public static final String URL_NEW_SERVICERECORD = URL_PREFIX + "/servicerecord";

    private static final DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.GERMAN);


    public static List<ServiceRecord> getAllServiceRecordsFromJson(String json) {
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(json);
        List<ServiceRecord> recordsList = new ArrayList<ServiceRecord>();

        if (element.isJsonArray()) {
            JsonArray records = element.getAsJsonArray();
            for (int i = 0; i < records.size(); i++) {
                JsonObject p = records.get(i).getAsJsonObject();
                int id = p.get("id").getAsInt();
                Date start = null;
                Date end = null;
                try {
                    start = format.parse(p.get("timeStart").getAsString());
                    end = format.parse(p.get("timeEnd").getAsString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String project = p.get("project").getAsJsonObject().get("name").getAsString();
                String service = p.get("service").getAsJsonObject().get("name").getAsString();

                recordsList.add(new ServiceRecord(id, start, end, service, project));
            }
        }
        return recordsList;
    }
}
