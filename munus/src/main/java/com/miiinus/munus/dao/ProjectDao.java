package com.miiinus.munus.dao;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.miiinus.munus.model.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 23.04.15
 */
public class ProjectDao extends BaseDao {
    public static final String URL_PROJECTS_WORKINGFOR = URL_PREFIX + "/projects/workingfor";

    public Project getProjectbyId(int id) {
        return null; //TODO impl
    }

    public static List<Project> getAllProjectsFromJson(String json) {
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(json);
        List<Project> projectList = new ArrayList<>();
        if (element.isJsonArray()) {
            JsonArray projects = element.getAsJsonArray();
            for (int i = 0; i < projects.size(); i++) {
                JsonObject p = projects.get(i).getAsJsonObject();

                int id = p.get("id").getAsInt();
                String name = p.get("name").getAsString();
                String description;

                try {
                    description = p.get("description").getAsString();
                }catch (UnsupportedOperationException e) {
                    description = "";
                }
                String customer = p.get("customer").getAsJsonObject().get("name").getAsString();

                projectList.add(new Project(id, name, description, customer));
            }
            Log.d("ProjectDAO", projectList.toString()); // TODO remove log
        }
        return projectList;
    }
}
