package com.miiinus.munus;

import com.miiinus.munus.model.Employee;

/**
 * Created by Robin on 23.04.15
 */
public class Session {

    private static Session instance;

    public Employee user;
    public String token;
    public static Session getInstance() {
        if (instance == null) {
            instance = new Session();
        }
        return instance;
    }

    /**
     * private default constructor
     */
    private Session() {
    }

    public void setToken(String token){
        this.token = "Bearer " + token;
    }

}