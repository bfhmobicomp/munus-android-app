package com.miiinus.munus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.miiinus.munus.R;
import com.miiinus.munus.model.Project;
import com.miiinus.munus.view.DashboardFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 25.05.15
 */
public class ProjectListAdapter extends ArrayAdapter {
    private DashboardFragment.OnItemSelectedListener mListener;
    private ArrayList<Project> mProjects;
    private Context mContext;

    public ProjectListAdapter(Context context, int resource, List<Project> projects) {
        super(context, resource, projects);
        this.mProjects = (ArrayList<Project>) projects;
        this.mContext = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.list_item_project, parent, false);
        }

        Project project = mProjects.get(position);
        if (project != null) {
            TextView tvTitle = (TextView) v.findViewById(R.id.liTitle);
            TextView tvDesc = (TextView) v.findViewById(R.id.liDesc);

            if (tvTitle != null) {
                tvTitle.setText(project.customer + " - " + project.name);
            }

            if (tvDesc != null) {
                tvDesc.setText(project.description);
            }
        }
        return v;
    }
    public void updateProjects(List<Project> newProjects) {
        this.mProjects.clear();
        this.mProjects.addAll(newProjects);
        notifyDataSetChanged();
    }

    public interface OnItemSelectedListener {
        public void onItemSelected(long id);
    } // TODO find out how renaming would work out


}
