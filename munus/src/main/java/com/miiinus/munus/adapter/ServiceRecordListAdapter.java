package com.miiinus.munus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.miiinus.munus.R;
import com.miiinus.munus.model.Customer;
import com.miiinus.munus.model.ServiceRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 25.05.15
 */
public class ServiceRecordListAdapter extends ArrayAdapter {
    private ArrayList<ServiceRecord> mRecords;
    private Context mContext;

    public ServiceRecordListAdapter(Context context, int resource, List<ServiceRecord> records) {
        super(context, resource, records);
        this.mRecords = (ArrayList<ServiceRecord>) records;
        this.mContext = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.list_item_servicerecord, parent, false);
        }

        ServiceRecord sr = mRecords.get(position);
        if (sr != null) {
            TextView tvService = (TextView) v.findViewById(R.id.liService);
            TextView tvTime = (TextView) v.findViewById(R.id.liTime);

            if (tvService != null) {
                tvService.setText(sr.service + " @ " + sr.project);
            }
            if (tvTime != null) {
                tvTime.setText(sr.start.toString() + " - " + sr.end.toString());
            }
        }
        return v;
    }
    public void updateServiceRecords(List<ServiceRecord> newServiceRecords) {
        this.mRecords.clear();
        this.mRecords.addAll(newServiceRecords);
        notifyDataSetChanged();
    }


}
