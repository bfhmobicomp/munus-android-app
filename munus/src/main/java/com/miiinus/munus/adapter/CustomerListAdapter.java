package com.miiinus.munus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.miiinus.munus.R;
import com.miiinus.munus.model.Customer;
import com.miiinus.munus.model.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 25.05.15
 */
public class CustomerListAdapter extends ArrayAdapter {
    private ArrayList<Customer> mCustomers;
    private Context mContext;

    public CustomerListAdapter(Context context, int resource, List<Customer> customers) {
        super(context, resource, customers);
        this.mCustomers = (ArrayList<Customer>) customers;
        this.mContext = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.list_item_customer, parent, false);
        }

        Customer customer = mCustomers.get(position);
        if (customer != null) {
            TextView tvName = (TextView) v.findViewById(R.id.liName);
            TextView tvInfo = (TextView) v.findViewById(R.id.liAddress);

            if (tvName != null) {
                tvName.setText(customer.name);
            }

            if(tvInfo != null) {
                // TODO intents for phone number and email
                String s = "";
                s += !customer.address.isEmpty() ? customer.address + "\n" : "";
                s += !customer.address.isEmpty() ? customer.phone + "\n" : "";
                s += !customer.email.isEmpty() ? customer.email : "";
                tvInfo.setText(s);
            }
        }
        return v;
    }
    public void updateCustomers(List<Customer> newCustomers) {
        this.mCustomers.clear();
        this.mCustomers.addAll(newCustomers);
        notifyDataSetChanged();
    }


}
