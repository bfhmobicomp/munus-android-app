package com.miiinus.munus.model;

import android.location.Address;
import java.util.List;
import java.util.ArrayList;

/**
 * Created by Robin on 17.04.15
 */

public class Enterprise {
    private int id;
    private String name;
    private Address address;
    private List<Service> services = new ArrayList<Service>();
    private List<Employee> employees = new ArrayList<Employee>();
    private List<Customer> customers = new ArrayList<Customer>();
}
