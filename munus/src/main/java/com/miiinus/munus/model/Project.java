package com.miiinus.munus.model;

import java.util.List;
import java.util.ArrayList;

/**
 * Created by Robin on 17.04.15
 */
public class Project {
    public int id;
    public String name;
    public String description;
    public String customer;
    public List<ServiceRecord> serviceRecords = new ArrayList<ServiceRecord>();

    /**
     * Constructor
     */
    public Project(int id, String name, String description, String customer) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.customer = customer;
    }

    /**
     * Adds a service-record to the project
     * @param sr the service-record to add
     */
    public void addServiceRecord(ServiceRecord sr) {
        this.serviceRecords.add(sr);
    }
}
