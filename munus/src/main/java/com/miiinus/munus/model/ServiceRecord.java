package com.miiinus.munus.model;

import java.util.Date;

/**
 * Created by Robin on 17.04.15
 */
public class ServiceRecord {
    private int id;
    public String service;
    public String project;
    public Date start;
    public Date end;
    private Employee creator;

    public ServiceRecord(int id, Date start, Date end, String service, String project) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.service = service;
        this.project = project;
    }
}
