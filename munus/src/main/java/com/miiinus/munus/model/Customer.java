package com.miiinus.munus.model;

import android.content.res.Resources;

import com.miiinus.munus.R;

import java.util.List;
import java.util.ArrayList;

/**
 * Created by Robin on 17.04.15
 */
public class Customer {
    public int id;
    public String name;
    public String address;
    public String phone;
    public String email;
    public List<Project> projects = new ArrayList<Project>();

    public Customer(int id, String name, String address, String phone, String email) {
        this.id = id;
        this.name = name;
        this.address = (!address.isEmpty()) ? address : "";
        this.phone = (!phone.isEmpty()) ? phone : "";
        this.email = (!email.isEmpty()) ? email : "";
    }
}
