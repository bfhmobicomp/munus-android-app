package com.miiinus.munus.tests;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 30.04.15
 */
public class Album {
        @SerializedName("Albumtitel")
        public String name;
        @SerializedName("Interpret")
        public String artist;
        @SerializedName("Jahr")
        public int year;
        @SerializedName("Songs")
        public List<Song> songs = new ArrayList<Song>();

        public Album() {

        }
}
