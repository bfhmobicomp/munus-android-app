package com.miiinus.munus.tests;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Robin on 17.04.15
 */
public class Album_Test {
    public static void main(String[] args) {
        // SET UP
        Album pchimbg = new Album();
        pchimbg.name = "Hello, I Must Be Going";
        pchimbg.artist = "Phil Collins";
        pchimbg.year = 1982;

        Song idca = new Song();
        idca.title = "I Dont Care Anymore";
        idca.length = 5.0333;
        pchimbg.songs.add(idca);

        // JAVA to JSON
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting().serializeNulls();
        Gson gson = builder.create();
        System.out.println(gson.toJson(pchimbg));
        String s = gson.toJson(pchimbg);

        // JSON to JAVA
        Album helloimusbegoing = gson.fromJson(s, Album.class);

        System.out.println("Album-Name: " + helloimusbegoing.name);
        System.out.println("Interpret: " + helloimusbegoing.artist);
        System.out.println("---Songs---");
        for (Song song : helloimusbegoing.songs) {
            System.out.println("Titel: " + song.title);
            System.out.println("Länge: " + song.length);
        }
    }
}
