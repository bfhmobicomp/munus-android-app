package com.miiinus.munus.tests;

import com.google.gson.GsonBuilder;
import com.google.gson.Gson;
import com.miiinus.munus.model.Employee;
import com.miiinus.munus.model.Project;
import com.miiinus.munus.model.Service;
import com.miiinus.munus.model.ServiceRecord;

import java.util.Date;

/**
 * Created by Robin on 17.04.15
 */
public class Gson_Test {
    public static void main(String[] args) {
        Employee mitarbeiter1 = new Employee("Ruedi");
        Service telefonieren = new Service("Telefonieren");
        Service entwickeln = new Service("Entwickeln");

        Date date1 = new Date(System.currentTimeMillis());
        Date date2 = new Date(System.currentTimeMillis()+10000000);
        Date date3 = new Date(System.currentTimeMillis()-10000000);

        ServiceRecord sr1 = new ServiceRecord(entwickeln, date1, date2, mitarbeiter1);
        ServiceRecord sr2 = new ServiceRecord(telefonieren, date3, date1, mitarbeiter1);

        Project project1 = new Project("Project 1", "This is a very important project");

        project1.addServiceRecord(sr1);
        project1.addServiceRecord(sr2);

        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting().serializeNulls();
        Gson gson = builder.create();

        System.out.println(gson.toJson(project1));
    }
}
