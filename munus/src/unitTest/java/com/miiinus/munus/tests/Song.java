package com.miiinus.munus.tests;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Robin on 30.04.15
 */
public class Song {
    @SerializedName("Liedtitel")
    public String title;
    @SerializedName("Länge")
    public Double length; //in minutes

    public Song() {

    }
}
